package javafpsession.functional;

import java.util.stream.IntStream;

public class Stateless
{

	//problem statement: print the sum of the squares of all the even numbers up to given range.
	public static void main(String[] args)
	{
		int range = 10;

		System.out.println(removingStateFP(range));

		// some more lines of code

		System.out.println(removingStateFP(range));

	}

	private static int removingStateFP(final int range)
	{
		return IntStream
				.rangeClosed(1, range)
				.parallel()
				.filter(num -> num % 2 != 0)
				.map(num -> num * num)
				.sum();
	}
}
