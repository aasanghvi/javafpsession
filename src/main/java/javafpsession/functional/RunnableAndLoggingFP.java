package javafpsession.functional;


import lombok.extern.log4j.Log4j2;

//problem statement: print the thread name from a runnable.
@Log4j2
public class RunnableAndLoggingFP
{
	public static void main(String[] args)
	{
		runnableLoggingFP();
	}

	private static void runnableLoggingFP()
	{
		new Thread(() -> log.debug("Running on thread: {}", () -> Thread.currentThread().getName())).start();
	}
}
