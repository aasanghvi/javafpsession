package javafpsession.functional;

import java.util.Map;

import static java.util.stream.Collectors.*;

// problem statement: given a map of each person and how many snickers they should give me,
// represent which people are corresponding to each count of snickers.
public class InvertMapFP
{
	public static void main(String[] args)
	{
		invertMapFP();
	}

	private static void invertMapFP()
	{
		System.out.println(Map
				.of("Raj", 3, "Vinita", 5, "Meet", 7, "Vishal", 1, "Prakshal", 3, "Preyas", 5, "Poojan", 1)
				.entrySet()
				.parallelStream()
				.collect(groupingBy(Map.Entry::getValue, mapping(Map.Entry::getKey, toList()))));
	}
}
