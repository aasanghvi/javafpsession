package javafpsession.functional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class TriplesFP
{
	public static void main(String[] args)
	{
		computeTriples(10).forEach(System.out::println);
	}

	public static String getTriple(int m, int n)
	{
		int a = m * m - n * n;
		int b = 2 * m * n;
		int c = m * m + n * n;

		return String.format("%d %d %d", a, b, c);
	}

	public static List<String> computeTriples(int limit)
	{
		return Stream.iterate(2, e -> e + 1)
				.flatMap(m -> IntStream.range(1, m).mapToObj(n -> getTriple(m, n)))
				.limit(limit)
				.collect(Collectors.toList());

	}

}
