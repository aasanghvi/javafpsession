package javafpsession.iterative;


public class Stateful
{

	private static int sum = 0;

	//problem statement: print the sum of the squares of all the even numbers up to given range.
	public static void main(String[] args)
	{
		int range = 5;

		stateful(range);
		System.out.println(sum);

		// some more lines of code

		stateful(range);
		System.out.println(sum);

	}

	private static int stateful(final int range)
	{

		for (int i = 1; i <= range; i++)
		{
			int square = i * i;
			if (i % 2 != 0)
			{
				sum += square;
			}
		}

		return sum;
	}

}
