package javafpsession.iterative;

import java.util.Arrays;
import java.util.List;

interface IStrategy
{
	int totalValues(List<Integer> numbers);
}

public class Strategy
{
	private IStrategy applyStrategy;

	public static void main(String[] args)
	{
		List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
		(new Strategy()).setStrategy(new TotalAllNumbers()).totalValues(numbers);
		(new Strategy()).setStrategy(new TotalOddNumbers()).totalValues(numbers);
		(new Strategy()).setStrategy(new TotalEvenNumbers()).totalValues(numbers);
	}

	public Strategy setStrategy(IStrategy applyStrategy)
	{
		this.applyStrategy = applyStrategy;
		return this;
	}

	public Integer totalValues(List<Integer> numbers)
	{
		if (applyStrategy == null)
		{
			throw new UnsupportedOperationException("Cannot perform this operation " +
					"because you did not set strategy yet!");
		}

		return applyStrategy.totalValues(numbers);
	}
}

class TotalAllNumbers implements IStrategy
{

	@Override
	public int totalValues(final List<Integer> numbers)
	{
		int sum = 0;

		for (int number : numbers)
		{
			sum += number;
		}

		return sum;
	}
}

class TotalOddNumbers implements IStrategy
{

	@Override
	public int totalValues(final List<Integer> numbers)
	{
		int sum = 0;

		for (int number : numbers)
		{
			sum += (number % 2 != 0) ? number : 0;
		}

		return sum;
	}
}

class TotalEvenNumbers implements IStrategy
{

	@Override
	public int totalValues(final List<Integer> numbers)
	{
		int sum = 0;

		for (int number : numbers)
		{
			sum += (number % 2 == 0) ? number : 0;
		}

		return sum;
	}
}

