package javafpsession.iterative;

import lombok.extern.log4j.Log4j2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//problem statement: print the thread name from a runnable.
@Log4j2
public class RunnableAndLogging
{
	static final Logger LOGGER = LoggerFactory.getLogger(RunnableAndLogging.class);

	public static void main(String[] args)
	{
		runnableLogging();
	}

	private static void runnableLogging()
	{
		new Thread(() -> log.debug("Running on thread: {}", () -> Thread.currentThread().getName())).start();
	}

}
