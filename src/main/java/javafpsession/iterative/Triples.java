package javafpsession.iterative;

import java.util.ArrayList;
import java.util.List;

// problem statement: print all the pythagorean triples up to given range n.
public class Triples
{

	/*
	 * Given
	 * a^2 + b^2 = c^2
	 * and given
	 * m^2 + 2mn + n^2 = (m+n)^2
	 * i.e
	 * m^4 + 2m^2n^2 + n^4 = (m^2 + n^2)^2
	 * if we substitute
	 * c = m^2 + n^2
	 * we get
	 * a^2 + b^2 = m^4 + 2m^2n^2 + n^4
	 * splitting 2m^2n^2 as (4m^2n^2 - 2m^2n^2)
	 * we get
	 * a^2 + b^2 = (m^4 - 2m^2n^2 + n^4) + 4m^2n^2
	 * a^2 + b^2 = (m^2 - n^2)^2 + (2mn)^2
	 * so that a, b and c are assumed as:
	 * a = m^2 - n^2
	 * b = 2mn
	 * c = m^2 + n^2
	 *
	 */


	public static void main(String[] args)
	{
		int range = 10;

		for (String str : computeTriples(range))
		{
			System.out.println(str);
		}

	}

	public static List<String> computeTriples(int limit)
	{
		List<String> triples = new ArrayList<>();
		int count = 1;

		for (int m = 2; ; m++)
		{
			for (int n = 1; n < m; n++)
			{
				triples.add(String.format("%d %d %d", m * m - n * n, 2 * m * n, m * m + n * n));
				count++;

				if (count > limit)
				{
					break;
				}
			}

			if (count > limit)
			{
				break;
			}
		}

		return triples;

	}

}
