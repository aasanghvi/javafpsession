package javafpsession.iterative;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// problem statement: given a map of each person and how many snickers they should give me,
// represent which people are corresponding to each count of snickers.
public class InvertMap
{
	public static void main(String[] args)
	{
		invertMap();

	}

	private static void invertMap()
	{
		Map<String, Integer> snickersMap = new HashMap<>();
		snickersMap.put("Raj", 3);
		snickersMap.put("Vinita", 5);
		snickersMap.put("Meet", 7);
		snickersMap.put("Vishal", 1);
		snickersMap.put("Prakshal", 3);
		snickersMap.put("Preyas", 5);
		snickersMap.put("Poojan", 1);

		Map<Integer, List<String>> invertedMap = new HashMap<>();

		for (Map.Entry<String, Integer> entry : snickersMap.entrySet())
		{
			List<String> names = (invertedMap.get(entry.getValue()) == null) ? new ArrayList<>() : invertedMap.get(entry.getValue());
			names.add(entry.getKey());
			invertedMap.put(entry.getValue(), names);
		}

		System.out.println(invertedMap);
	}

}
